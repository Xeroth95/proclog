(ns proclog.util)

(defn member?
  "Tests if `elem` is contained in the sequence `list`."
  [elem list]
  (some #{elem} list))

(defn get-unused-keyword
  "Generates a new and unused keyword"
  []
  (letfn [(gen-keyword [candidate] (if (find-keyword candidate) (recur (name (gensym))) (keyword candidate)))]
    (gen-keyword (name (gensym)))))
