(ns proclog.interface
  (:require [clojure.string :as str])
  (:use proclog.core))

(defn print-mgu [state]
  (if (:mgu state)
    (doall (map (fn [[var value]] (println (name var) " = " value)) (:mgu state))))
  state)

(defn yes-or-no [state]
  (print-mgu state)
  (if (empty? (:bt-stack state))
    state
    (do (println "Yes / No:")
        (loop [answer (clojure.string/lower-case (read-line))]
          (cond
            (str/starts-with? "yes" answer) state
            (str/starts-with? "no" answer) (update state :mgu (constantly false))
            true (do (println "Enter \"Yes\" or \"No\":")
                     (recur (clojure.string/lower-case (read-line)))))))))

(defmacro proof [& body]
  `(boolean (bt-do {:mgu {},
                    :forms nil,
                    :bt-stack nil
                    :depth 0}
                   ~@body
                   yes-or-no)))

(defn conj* [coll x]
  (if (empty? coll)
    x
    (concat coll (list x))))

(defn cut-mgu [state vars]
  (update state :mgu #(reduce dissoc % vars)))

(defmacro <- [name [& params] & body]
  (let [vars (undefined-variables params body)
        funs (undefined-functions body)
        state (gensym)
        declaration `(defn ~name ~(into [state] params))
        body `(bt-do ~state ~@(transform-body body) (cut-mgu ~vars))
        declare-funs (if (empty? funs) nil `(do (declare ~@funs)))
        declare-vars (if (empty? vars) nil `(let ~(undefined-variable-definitions vars)))]
    ;; This may seem weird but it is done to make
    ;; the outputted code much clearer to read
    ;; since (let [] X) and (do Y)
    ;; are reduced to just X and Y.
    (->> body
         (conj* declare-vars)
         (conj* declaration)
         (conj* declare-funs))))
